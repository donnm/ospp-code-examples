/*
 * stopwatch.c
 *
 * Donn Morrison 2019
 *
 * License: Public domain
 *
 * Simple stopwatch using gettimeofday()
 *
 */

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    struct timeval t1, t2;

    if(argc < 2)
    {
        printf("usage: %s <command>\n", argv[0]);
        exit(1);
    }

    gettimeofday(&t1, NULL);
    system(argv[1]);
    gettimeofday(&t2, NULL);

    printf("elapsed time: %d seconds, %d usec\n", t2.tv_sec-t1.tv_sec,t2.tv_usec-t1.tv_usec);
    exit(0);
}
