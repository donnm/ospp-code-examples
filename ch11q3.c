/* OSPP Chapter 11 question 3
 * 
 * Donn Morrison 2019
 *
 * License: Public domain
 * 
 * Create a new file
 * Write 100KB to it
 * Flush the writes
 * Delete the file
 * Time these steps
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

unsigned char *generate_data(int size)
{
    // Generate random data
    unsigned char *data = malloc(size * sizeof(unsigned char));
    if(data == NULL)
        return NULL;
    int randfd = open("/dev/urandom", 'r');
    int bytes_read = read(randfd, data, size);
    if(bytes_read != size)
    {
        free(data);
        return NULL;
     }

    close(randfd);
    return data;
}


int main(int argc, char *argv[])
{
    unsigned char *data = generate_data(100 * 1024);
    unsigned char *ptr = data;

    if(data == NULL)
    {
        printf("Could not read data\n");
        exit(1);
    }

    // Print to stdout
    while(ptr - data < 100 * 1024)
    {
       // printf("%c", *ptr);
        *ptr++;
    }

    // Create a new file
    int fd = open("perf.bin", O_CREAT | O_RDWR, 0666);
    if(fd < 0)
    {
        perror("open() failed\n");
        exit(1);
    }
    
    // Write 100KB to it

    int bytes_written = write(fd, data, 100 * 1024);
    if(bytes_written != 100 * 1024)
    {
        perror("write() failed\n");
        exit(1);
    }

    // Flush the writes
    fsync(fd);

    // Close the file descriptor
    close(fd);

    // Check our result
    system("ls -l");

    // Delete the file
    if(unlink("perf.bin") < 0)
    {
        perror("unlink() failed\n");
        exit(1);
    }

    // Time these steps
    // gettimeofday()

    free(data);
    exit(0);
}
